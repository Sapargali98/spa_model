async function fetchAndDisplayPosts() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const posts = await response.json();
        const postsList = document.getElementById('posts-list');
        postsList.innerHTML = '';
        posts.forEach(post => {
            const postItem = document.createElement('div');
            postItem.innerHTML = `
                </div>
                <div class="post-image-container">
                <img src="" alt="Post Image" class="post-image" id="post-image-${post.id}">
                </div>
                <h3>${post.title}</h3>
                <p>${post.body}</p>      
                <div class="comment-section">
                    <h4>Comments:</h4>
                    <form id="comment-form-${post.id}">
                        <input type="text" placeholder="Add a comment..." class="comment-input" id="comment-input-${post.id}">
                        <button type="submit">Post</button>
                    </form>
                    <div class="comments-list" id="comments-list-${post.id}"></div>
                <form id="image-form-${post.id}" class="image-upload-form">
                <input type="file" id="image-input-${post.id}">
                <button type="submit">Upload Image</button>
                </form>
                <button class="like-button" id="like-button-${post.id}" data-liked="false" data-like-count="0">Like</button>
                <span class="like-count" id="like-count-${post.id}">0</span> Likes
            `;
            const editButton = document.createElement('button');
            editButton.textContent = 'Edit';
            editButton.addEventListener('click', () => openEditModal(post));
            postItem.appendChild(editButton);

            const deleteButton = document.createElement('button');
            deleteButton.textContent = 'Delete';
            deleteButton.addEventListener('click', () => deletePost(post.id));
            postItem.appendChild(deleteButton);
            const likeButton = document.createElement('button');
            likeButton.textContent = 'Like';
            likeButton.className = 'like-button';
            likeButton.id = `like-button-${post.id}`;
            likeButton.setAttribute('data-liked', 'false');
            likeButton.setAttribute('data-like-count', '0');
            postItem.appendChild(likeButton);
            
            likeButton.addEventListener('click', () => toggleLike(post.id));
            
            postsList.appendChild(postItem);
            const commentForm = document.getElementById(`comment-form-${post.id}`);
            commentForm.addEventListener('submit', (event) => {
                event.preventDefault();
                addCommentToPost(post.id); 
            const imageForm = document.getElementById(`image-form-${post.id}`);
            imageForm.addEventListener('submit', (event) => {
                    event.preventDefault();
                    uploadImage(post.id); 
                });
            });
        });
    } catch (error) {
        console.error('There was a problem with your fetch operation:', error);
    }
}
async function createPost() {
    const title = document.getElementById('postTitle').value;
    const body = document.getElementById('postBody').value;

    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: title,
                body: body,
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        await response.json();
        fetchAndDisplayPosts();
    } catch (error) {
        console.error('There was a problem with your fetch operation:', error);
        alert('Error creating post: ' + error.message);
    }
}
function openEditModal(post) {
    document.getElementById('editPostTitle').value = post.title;
    document.getElementById('editPostBody').value = post.body;
    document.getElementById('editPostId').value = post.id;
    document.getElementById('editPostModal').style.display = 'block';
}

async function deletePost(id) {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: 'DELETE',
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        fetchAndDisplayPosts();
    } catch (error) {
        console.error('There was a problem with your fetch operation:', error);
        alert('Error deleting post: ' + error.message);
    }
}

function showSection(sectionId) {
    document.querySelectorAll('.section').forEach(section => {
        section.style.display = 'none';
    });
    document.getElementById(sectionId).style.display = 'block';
}

document.addEventListener('DOMContentLoaded', () => {
    fetchAndDisplayPosts();
    showSection('posts-list');
});

document.getElementById('create-post-form').addEventListener('submit', event => {
    event.preventDefault();
    createPost();
});
function searchPosts() {
    const searchTerm = document.getElementById('search-input').value.toLowerCase();
    const postsList = document.getElementById('posts-list');
    Array.from(postsList.children).forEach(postItem => {
        const title = postItem.querySelector('h3').textContent.toLowerCase();
        const body = postItem.querySelector('p').textContent.toLowerCase();
        const isVisible = title.includes(searchTerm) || body.includes(searchTerm);
        postItem.style.display = isVisible ? 'block' : 'none';
    });
}

function closeEditModal() {
    document.getElementById('editPostModal').style.display = 'none';
}

async function updatePost() {
    const postId = document.getElementById('editPostId').value;
    const title = document.getElementById('editPostTitle').value;
    const body = document.getElementById('editPostBody').value;

    const postData = {
        title: title,
        body: body
    };

    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'PUT', 
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
            body: JSON.stringify(postData)
        });

        if (!response.ok) {
            throw new Error('Network response was not ok');
        }

        const updatedPost = await response.json();
        console.log('Updated post:', updatedPost);

        fetchAndDisplayPosts();
        closeEditModal();
    } catch (error) {
        console.error('Error updating post:', error);
        alert('Error updating post: ' + error.message);
    }
}
function addCommentToPost(postId) {
    const commentInput = document.getElementById(`comment-input-${postId}`);
    const commentText = commentInput.value.trim();
    if (commentText) {
        const commentsList = document.getElementById(`comments-list-${postId}`);
        const commentItem = document.createElement('div');
        commentItem.innerHTML = `<p><strong>User</strong>: ${commentText}</p>`;
        commentsList.appendChild(commentItem);
        commentInput.value = '';
    }
}
async function uploadImage(postId) {
    const imageInput = document.getElementById(`image-input-${postId}`);
    const formData = new FormData();
    formData.append('image', imageInput.files[0]);

    try {
        const response = await fetch('URL_TO_UPLOAD_IMAGE_API', {
            method: 'POST',
            body: formData,
        });

        if (!response.ok) {
            throw new Error('Image upload failed');
        }

        const result = await response.json();
        const postImage = document.getElementById(`post-image-${postId}`);
        postImage.src = result.imageUrl; 
    } catch (error) {
        console.error('Error uploading image:', error);
    }
}
function toggleLike(postId) {
    const likeButton = document.getElementById(`like-button-${postId}`);
    const likeCount = document.getElementById(`like-count-${postId}`);
    const isLiked = likeButton.getAttribute('data-liked') === 'true';
    const currentLikeCount = parseInt(likeButton.getAttribute('data-like-count'));

    if (isLiked) {
        likeButton.setAttribute('data-liked', 'false');
        likeButton.classList.remove('liked');
        likeCount.textContent = currentLikeCount - 1;
        likeButton.setAttribute('data-like-count', currentLikeCount - 1);
    } else {
        likeButton.setAttribute('data-liked', 'true');
        likeButton.classList.add('liked');
        likeCount.textContent = currentLikeCount + 1;
        likeButton.setAttribute('data-like-count', currentLikeCount + 1);
    }
}

document.addEventListener('DOMContentLoaded', fetchAndDisplayPosts);

